module Corpusbuilder::Train
  class Compile < Run
    attr_accessor :model_name

    def execute
      puts compile_command.strip.gsub(/\s+/, ' ').gsub(/--/, "\n             --").yellow
      puts ""

      `#{compile_command}`
    end

    def compile_command
      if last_checkpoint_filename.nil?
        fail "Couldn't find the #{last_checkpoint_filename} file. Did you run the training process first?"
      end

      <<-command
          lstmtraining --traineddata #{traineddata_file_path} \
                       --continue_from #{last_checkpoint_filename} \
                       --model_output #{model_output_path} \
                       --stop_training
      command
    end

    def model_output_path
      memoized do
        FileUtils.mkdir_p File.join(model_path, "output")

        File.join(model_path, "output", model_name + ".traineddata")
      end
    end

    def traineddata_file_path
      memoized do
        File.join(model_path, "combined", "#{model_name}.traineddata").tap do |path|
          if !File.exist? path
            fail "Couldn't find the #{path} file. Did you run the training process first?"
          end
        end
      end
    end
  end
end

