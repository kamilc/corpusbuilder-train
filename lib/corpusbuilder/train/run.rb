require 'pty'
require 'powerbar'
require 'colored'

module Corpusbuilder::Train
  class Run < Action
    attr_accessor :model_name

    before :validate_model_name

    def execute
      puts train_command.strip.gsub(/\s+/, ' ').gsub(/--/, "\n             --").yellow
      puts ""

      PTY.spawn train_command do |stdout, stdin, pid|
        begin
          total = 10000
          current = 0
          progress = PowerBar.new

          progress.settings.tty.finite.template.padchar = '.'
          progress.settings.tty.finite.template.barchar = '*'

          stdout.each do |line|
            word_train = line[/word train=[^,]*/]

            if !word_train.nil?
              current = total - (word_train.gsub(/word train=/, '').gsub('%', '').to_f * 100).round
            end

            progress.show({:msg => 'Word Accuracy', :done => current, :total => total}) if current != 0

            logger.info line
          end
        rescue Errno::EIO
          puts "Finished"
        end
      end
    end

    def train_command
      memoized do
        <<-command
          lstmtraining --traineddata #{traineddata_file_path} \
                      #{ last_checkpoint_filename.nil? ? "" : "--continue_from #{last_checkpoint_filename}" } \
                       --net_spec \"#{network_spec}\" \
                       --model_output #{model_output_path} \
                       --train_listfile #{train_list_file_path} \
                       --eval_listfile #{eval_list_file_path}
        command
      end
    end

    def eval_list_file_path
      memoized do
        File.join(model_path, "list.train").tap do |path|
          if !File.exist? path
            `cat #{all_lstmf_file_path} | head -n #{training_lstmf_files_num} > #{path}`
          end
        end
      end
    end

    def train_list_file_path
      memoized do
        File.join(model_path, "list.eval").tap do |path|
          if !File.exist? path
            `cat #{all_lstmf_file_path} | tail -n +#{eval_lstmf_files_num} > #{path}`
          end
        end
      end
    end

    def all_lstmf_file_path
      memoized do
        File.join(model_path, "all-lstmf").tap do |path|
          if !File.exist? path
            File.open path, "w" do |file|
              lstmf_files_list.each do |file_path|
                file.puts file_path
              end
            end
          end
        end
      end
    end

    def lstmf_files_list
      memoized do
        total = tif_files_list.count
        current = 0
        progress = PowerBar.new

        progress.settings.tty.finite.template.padchar = '.'
        progress.settings.tty.finite.template.barchar = '*'

        tif_files_list.map do |tif_file_path|
          progress.show({:msg => 'Creating LSTMF files', :done => current, :total => total})
          current += 1
          tif_file_path.gsub(/.tif$/, '.lstmf').tap do |path|
            if !File.exist? path
              PTY.spawn "TESSDATA_PREFIX=#{tessdata_prefix} tesseract #{tif_file_path} #{path.gsub(/.lstmf$/, '')} lstm.train" do |stdout, stdin, pid|
                begin
                  stdout.each do |line|
                    logger.info line
                  end
                rescue Errno::EIO
                end
              end

              if $?.exitstatus == 0 && !File.exist?(path)
                FileUtils.rm path.gsub(/.lstmf$/, '.tif')
              end
            end
          end
        end
      end
    end

    def tif_files_list
      memoized do
        extracted_archive_paths.inject([]) do |state, path|
          state + Dir.glob(File.join(path, "*.tif"))
        end
      end
    end

    def box_files_list
      memoized do
        tif_files_list.map { |path| path.gsub(/.tif$/, '.box') }
      end
    end

    def extracted_archive_paths
      memoized do
        model_config["archives"].map do |url|
          name = url.split("/").last

          File.join(store_path, "data", name.gsub(/.bz2$/, '')).tap do |path|
            if !File.exist? path
              download url, name
              FileUtils.mkdir_p path
              `tar xjf #{File.join(tmp_path, name)} -C #{path}`
            end
          end
        end
      end
    end

    def all_examples_num
      memoized do
        lstmf_files_list.count
      end
    end

    def training_lstmf_files_num
      memoized do
        (model_config["ratio"].to_f * all_examples_num).round
      end
    end

    def eval_lstmf_files_num
      memoized do
        all_examples_num - training_lstmf_files_num
      end
    end

    def last_checkpoint_filename
      path = File.join(model_path, "checkpoints", model_name + "_checkpoint")

      if File.exist? path
        path
      else
        nil
      end
    end

    def model_output_path
      memoized do
        File.join(model_path, "checkpoints", model_name).tap do |path|
          if !File.exist?(path)
            FileUtils.mkdir_p path
          end
        end
      end
    end

    def network_spec
      memoized do
        model_config["network_spec"].gsub('{num_classes}', num_classes.to_s)
      end
    end

    def model_config
      memoized do
        YAML.load_file model_config_path
      end
    end

    def num_classes
      memoized do
        result = 0

        File.open unicharset_path, "r" do |file|
          result = file.each.first.to_i
        end

        result
      end
    end

    def unicharset_path
      memoized do
        File.join(model_combined_files_path, "unicharset").tap do |path|
          if !File.exist? path
            IO.write path, unicharset_contents
          end
        end
      end
    end

    def unicharset_contents
      memoized do
        compose UnicharsetExtractor,
          box_files_list: box_files_list
      end
    end

    def model_combined_files_path
      memoized do
        File.join(model_path, "combined").tap do |path|
          FileUtils.mkdir_p path
        end
      end
    end

    def traineddata_file_path
      memoized do
        File.join(model_path, "combined", "#{model_name}.traineddata").tap do |path|
          if !File.exist? path
            combine_command = <<-eos
              combine_lang_model \
                --input_unicharset #{unicharset_path} \
                --script_dir #{tessdata_prefix} \
                --output_dir #{model_combined_files_path} \
                #{ lang_is_rtl ? "--lang_is_rtl" : "" } \
                --pass_through_recoder \
                --lang #{model_name}
            eos

            `#{combine_command}`
            `mv #{File.join(model_combined_files_path, model_name, "*")} #{model_combined_files_path}`

            if $?.exitstatus != 0
              fail "Failed to combine the language model"
            end
          end
        end
      end
    end

    def lang_is_rtl
      model_config["lang_is_rtl"]
    end

    def model_path
      memoized do
        File.join(store_path, "models", model_name)
      end
    end

    def model_config_path
      memoized do
        File.join(model_path, "config.yml")
      end
    end

    def validate_model_name
      if !File.exist?(model_path)
        fail "The #{model_path} directory doesn't exist"
      end

      if !File.exist?(model_config_path)
        fail "The #{model_config_path} config file doesn't exist"
      end
    end
  end
end
