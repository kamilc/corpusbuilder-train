require 'net/http'
require 'yaml'
require 'open3'

module Corpusbuilder::Train
  class Action
    attr_accessor :store_config, :store_path

    def self.run(path, options = {})
      instance = new

      instance.instance_variable_set :@__path, path
      instance.instance_variable_set :@__options, options

      store_config = YAML::load_file(File.join("config.yml"))

      options.each do |name, value|
        instance.instance_variable_set "@#{name}".to_sym, value
      end

      instance.store_config = store_config
      instance.store_path = "."

      (@__before || []).each { |s| instance.send s }

      result = instance.execute

      (@__after || []).each { |s| instance.send s }

      result
    end

    def self.before(*method_symbols)
      @__before ||= []

      method_symbols.each do |method|
        @__before << method
      end
    end

    def self.after(*method_symbols)
      @__after ||= []

      method_symbols.each do |method|
        @__after << method
      end
    end

    def compose(klass, options = {})
      klass.run(@__path, @__options.merge(options))
    end

    def memoized(options = {}, &block)
      loc = caller_locations(1, 1).first
      @_memoized ||= {}

      if @_memoized["#{loc.path}:#{loc.lineno}:nil"]
        return nil
      end

      @_memoized["#{loc.path}:#{loc.lineno}"] ||= -> {
        ret = block.call

        if ret.nil?
          @_memoized["#{loc.path}:#{loc.lineno}:nil"] = true
        end

        options.fetch(:freeze, false) ? ret.freeze : ret
      }.call
    end

    def tessdata_prefix
      memoized do
        File.join(store_path, "tessdata").tap do |path|
          if !File.exist? path
            tesseract_file_name = "629a10b504af8c3c5ce922962f1d08abf001b1de.zip"
            langdata_file_name = "106c9b31bea9d30814fc116cbcb9c267dee7df70.zip"
            models_file_name = "f8c44498f369e9b2ca9728ad213f1e1f8b438a44.zip"

            out_path = File.join tmp_path, langdata_file_name
            models_out_path = File.join tmp_path, models_file_name
            tesseract_out_path = File.join tmp_path, tesseract_file_name

            download "https://github.com/tesseract-ocr/tesseract/archive/#{tesseract_file_name}", tesseract_file_name
            download "https://github.com/tesseract-ocr/langdata/archive/#{langdata_file_name}", langdata_file_name
            download "https://github.com/tesseract-ocr/tessdata_best/archive/#{models_file_name}", models_file_name

            `unzip #{tesseract_out_path} -d #{tmp_path}`

            if $?.exitstatus == 0
              `mv #{File.join(tmp_path, "tesseract*", "tessdata")} #{store_path}`
            else
              fail "Failed to unzip to #{store_path}"
            end

            `unzip #{out_path} -d #{tmp_path}`

            if $?.exitstatus == 0
              `mv #{File.join(tmp_path, "langdata-*", "*")} #{path}/`
            else
              fail "Failed to unzip to #{tmp_path}"
            end

            `unzip #{models_out_path} -d #{tmp_path}`

            if $?.exitstatus == 0
              `mv #{File.join(tmp_path, "tessdata*", "*")} #{path}/`
              `rm -fr #{File.join(tmp_path, "tessdata_best*")}`
            else
              fail "Failed to unzip to #{tmp_path}"
            end
          end
        end
      end
    end

    def logger
      memoized do
        Logger.new(File.open(File.join(store_path, "train.log"), "a"))
      end
    end

    def download(url, name)
      out_path = File.join(tmp_path, name)

      if !File.exist? out_path
        `wget #{url} -O #{out_path}`

        if $?.exitstatus != 0
          fail "Filed to download from #{url}"
        end
      end
    end

    def tmp_path
      memoized do
        File.join(store_path, "tmp").tap do |path|
          FileUtils.mkdir_p path
        end
      end
    end

    def execute
      raise NotImplementedError
    end
  end
end


