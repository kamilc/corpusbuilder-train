
lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "corpusbuilder/train/version"

Gem::Specification.new do |spec|
  spec.name          = "corpusbuilder-train"
  spec.version       = Corpusbuilder::Train::VERSION
  spec.authors       = ["Kamil Ciemniewski"]
  spec.email         = ["kamil@ciemniew.ski"]
  spec.summary       = "Training models for CorpusBuilder"

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end

  spec.bindir        = "exe"
  spec.executables   = `git ls-files -- exe/*`.split("\n").map{ |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.16"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.0"
  spec.add_development_dependency "byebug"

  spec.add_dependency "thor", "~> 0.20"
  spec.add_dependency "unicode-scripts"
  spec.add_dependency "unicode-categories"
  spec.add_dependency "powerbar"
  spec.add_dependency "colored"
end
