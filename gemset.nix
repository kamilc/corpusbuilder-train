{
  byebug = {
    source = {
      remotes = ["https://rubygems.org"];
      sha256 = "10znc1hjv8n686hhpl08f3m2g6h08a4b83nxblqwy2kqamkxcqf8";
      type = "gem";
    };
    version = "10.0.2";
  };
  colored = {
    source = {
      remotes = ["https://rubygems.org"];
      sha256 = "0b0x5jmsyi0z69bm6sij1k89z7h0laag3cb4mdn7zkl9qmxb90lx";
      type = "gem";
    };
    version = "1.2";
  };
  corpusbuilder-train = {
    dependencies = ["colored" "powerbar" "thor" "unicode-categories" "unicode-scripts"];
    source = {
      url = "https://kamilc@bitbucket.org/kamilc/corpusbuilder-train.git";
      rev = "e5f0422c3abd1bc723f9384f4f4040a9dbff9344";
      fetchSubmodules = false;
      sha256 = "1cb7zi8jkbmglnrff22dkn3i1fc27z4k75mwyr9lhbjv9nfsxzpz";
      type = "git";
    };
    version = "0.0.1";
  };
  diff-lcs = {
    source = {
      remotes = ["https://rubygems.org"];
      sha256 = "18w22bjz424gzafv6nzv98h0aqkwz3d9xhm7cbr1wfbyas8zayza";
      type = "gem";
    };
    version = "1.3";
  };
  hashie = {
    source = {
      remotes = ["https://rubygems.org"];
      sha256 = "1hh5lybf8hm7d7xs4xm8hxvm8xqrs2flc8fnwkrclaj746izw6xb";
      type = "gem";
    };
    version = "3.5.7";
  };
  powerbar = {
    dependencies = ["hashie"];
    source = {
      remotes = ["https://rubygems.org"];
      sha256 = "0dzbcgnqndlcsm09lnmpbbrq7sr3hqisld1v61iy2fzgjnwj82if";
      type = "gem";
    };
    version = "2.0.1";
  };
  rake = {
    source = {
      remotes = ["https://rubygems.org"];
      sha256 = "0jcabbgnjc788chx31sihc5pgbqnlc1c75wakmqlbjdm8jns2m9b";
      type = "gem";
    };
    version = "10.5.0";
  };
  rspec = {
    dependencies = ["rspec-core" "rspec-expectations" "rspec-mocks"];
    source = {
      remotes = ["https://rubygems.org"];
      sha256 = "0134g96wzxjlig2gxzd240gm2dxfw8izcyi2h6hjmr40syzcyx01";
      type = "gem";
    };
    version = "3.7.0";
  };
  rspec-core = {
    dependencies = ["rspec-support"];
    source = {
      remotes = ["https://rubygems.org"];
      sha256 = "0zvjbymx3avxm3lf8v4gka3a862vnaxldmwvp6767bpy48nhnvjj";
      type = "gem";
    };
    version = "3.7.1";
  };
  rspec-expectations = {
    dependencies = ["diff-lcs" "rspec-support"];
    source = {
      remotes = ["https://rubygems.org"];
      sha256 = "1fw06wm8jdj8k7wrb8xmzj0fr1wjyb0ya13x31hidnyblm41hmvy";
      type = "gem";
    };
    version = "3.7.0";
  };
  rspec-mocks = {
    dependencies = ["diff-lcs" "rspec-support"];
    source = {
      remotes = ["https://rubygems.org"];
      sha256 = "0b02ya3qhqgmcywqv4570dlhav70r656f7dmvwg89whpkq1z1xr3";
      type = "gem";
    };
    version = "3.7.0";
  };
  rspec-support = {
    source = {
      remotes = ["https://rubygems.org"];
      sha256 = "1nl30xb6jmcl0awhqp6jycl01wdssblifwy921phfml70rd9flj1";
      type = "gem";
    };
    version = "3.7.1";
  };
  thor = {
    source = {
      remotes = ["https://rubygems.org"];
      sha256 = "0nmqpyj642sk4g16nkbq6pj856adpv91lp4krwhqkh2iw63aszdl";
      type = "gem";
    };
    version = "0.20.0";
  };
  unicode-categories = {
    source = {
      remotes = ["https://rubygems.org"];
      sha256 = "1df6zb1cyzi1ij5kczan4b3hbx48bb6rbxdgrai755vpvyxan0k5";
      type = "gem";
    };
    version = "1.3.0";
  };
  unicode-scripts = {
    source = {
      remotes = ["https://rubygems.org"];
      sha256 = "13ylkaqgvciwbrp3dra41ihmqwc1mls07sky1nxnh11l422v7b8d";
      type = "gem";
    };
    version = "1.3.0";
  };
}
