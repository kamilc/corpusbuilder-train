{
  nixpkgs ? import <nixpkgs> { }
}:

with nixpkgs;

stdenv.mkDerivation rec {
  name = "tesseract-${version}";
  version = "4.00.00beta-git";

  src = fetchFromGitHub {
    owner = "tesseract-ocr";
    repo = "tesseract";
    rev = "9ae97508aed1e5508458f1181b08501f984bf4e2";
    sha256 = "0jhv2h9wqylmk8l56sc9z30pw8ddh1cxyincmn1vl5wr0m3c29j7";
  };

  nativeBuildInputs = [ pkgconfig autoreconfHook autoconf-archive ];
  buildInputs = [ leptonica libpng libtiff icu pango opencl-headers ];

  postBuild =''
  '';

  postInstall =''
    make training
    make training-install
  '';

  postDist = ''
    # find src/training/ -name "*.o" | xargs -I @ cp @ $out/bin
  '';

  meta = {
    description = "OCR engine";
    homepage = https://github.com/tesseract-ocr/tesseract;
    license = stdenv.lib.licenses.asl20;
    maintainers = with stdenv.lib.maintainers; [viric];
    platforms = with stdenv.lib.platforms; linux;
  };
}
