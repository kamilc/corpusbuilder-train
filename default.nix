with (import <nixpkgs> {});

bundlerApp {
  pname = "corpusbuilder-train";
  gemdir = ./.;
  exes = [ "corpusbuilder-train" ];

  meta = with lib; {
    description = "Easy training of Tesseract models based on datasets from CorpusBuilder";
    license     = licenses.mit;
    platforms   = platforms.unix;
  };
}
